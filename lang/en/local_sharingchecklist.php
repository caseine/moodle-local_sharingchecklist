<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_sharingchecklist', language 'en'
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Checklist for sharing a course';

$string['modulesshared'] = 'Modules shared: <b>{$a->n}</b> ({$a->percent}%). You can use the "Metadata Status" block to track shared modules within a course.';
$string['sharingcartconfigured'] = 'Sharing Cart block is <a href="{$a->link}">fully configured</a>.';
$string['sharingcartnotconfigured'] = 'Sharing Cart block is <a href="{$a->link}">not fully configured</a>.';
$string['sharingcartnotpresent'] = 'Sharing Cart block is not present on course page.';
$string['sharingcartnotvisible'] = 'Sharing Cart block is not visible.';
$string['sharingcartpresent'] = 'Sharing Cart block is present on course page.';
$string['sharingcartvisible'] = 'Sharing Cart block is visible.';
$string['sharingchecklist'] = 'Course sharing checklist';
$string['turnonediting'] = 'Please <b>turn editing on</b> for the quicklinks to function.';
$string['coursestatuscheck'] = 'This course is correctly shared! It will appear as shared on the courses lists. It is still recommended to review the points marked as warnings above.';
$string['coursestatusfail'] = 'This course is not correctly shared. It will not appear as shared on the courses lists.';
$string['coursestatuspass'] = 'This course is correctly shared! It will appear as shared on the courses lists.';
$string['enrolinactive'] = 'Found {$a->n} <a href="{$a->link}">enrolment instance(s)</a> for "{$a->role}" role, but none active.';
$string['enrolmultiple'] = 'More than one active <a href="{$a->link}">enrolment instance</a> exists for "{$a->role}" role. It is recommended to only set one.';
$string['enrolnone'] = 'No Temporary Access <a href="{$a->link}">enrolment instance</a> found for "{$a->role}" role with "local/sharedspaceh:accesstospaceh" required capability.';
$string['enrolnonrecommendedcapabilities'] = '<a href="{$a->link}">Temporary access for "{$a->role}" role</a> has non-recommended capabilities setting. It is recommended to set the required capabilities to "local/sharedspaceh:accesstospaceh".';
$string['enrolnottemporaryaccess'] = 'At least one active <a href="{$a->link}">enrolment instance</a> exists for "{$a->role}" role, but it is recommended to use the Temporary Access enrolment method.';
$string['enrolok'] = '<a href="{$a->link}">Temporary access for "{$a->role}" role</a> is correctly configured.';
$string['filtersharedcourses'] = 'Filter by shared courses';

$string['privacy:metadata'] = 'This plugin stores no personal data.';

$string['setting:filterlocations'] = 'Show filter by shared courses button on';
$string['setting:filterlocations_desc'] = 'A button allowing users to filter courses by shared status will be added at the selected locations.';
$string['setting:courseindex'] = 'Courses list';
$string['setting:courseindexcategory'] = 'Courses list by category';
$string['setting:coursesearch'] = 'Courses search result';
$string['setting:siteindex'] = 'Courses combo list on front page';
