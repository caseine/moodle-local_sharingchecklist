<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_sharingchecklist', language 'fr'
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Checklist pour le partage de cours';

$string['modulesshared'] = 'Modules partagés : <b>{$a->n}</b> ({$a->percent}%). Vous pouvez utiliser le bloc "Metadata Status" pour visualiser les modules partagés dans un cours.';
$string['sharingcartconfigured'] = 'Le bloc Panier d\'Activités est <a href="{$a->link}">entièrement configuré</a>.';
$string['sharingcartnotconfigured'] = 'Le bloc Panier d\'Activités n\'est <a href="{$a->link}">pas entièrement configuré</a>.';
$string['sharingcartnotpresent'] = 'Le bloc Panier d\'Activités n\'est pas présent sur la page de cours.';
$string['sharingcartnotvisible'] = 'Le bloc Panier d\'Activités n\'est pas visible.';
$string['sharingcartpresent'] = 'Le bloc Panier d\'Activités est présent sur la page de cours.';
$string['sharingcartvisible'] = 'Le bloc Panier d\'Activités est visible.';
$string['sharingchecklist'] = 'Checklist de partage du cours';
$string['turnonediting'] = 'Veuillez <b>activer le mode édition</b> pour que tous les liens d\'accès rapide fonctionnent correctement.';
$string['coursestatuscheck'] = 'Ce cours est correctement partagé ! Il apparaîtra comme étant partagé dans les listes de cours. Il est tout de même recommandé de revoir les points marqués comme avertissements ci-dessus.';
$string['coursestatusfail'] = 'Ce cours n\'est pas correctement partagé. Il n\'apparaîtra pas comme étant partagé dans les listes de cours.';
$string['coursestatuspass'] = 'Ce cours est correctement partagé ! Il apparaîtra comme étant partagé dans les listes de cours.';
$string['enrolinactive'] = '{$a->n} <a href="{$a->link}">méthode(s) d\'inscription</a> trouvées pour le rôle "{$a->role}", mais aucune n\'est active.';
$string['enrolmultiple'] = 'Il existe plus d\'une <a href="{$a->link}">méthode d\'inscription</a> pour le rôle "{$a->role}". Il est recommandé de n\'en avoir qu\'une.';
$string['enrolnone'] = 'Aucune <a href="{$a->link}">méthode d\'inscription</a> de type Temporary Access trouvée pour le rôle "{$a->role}" avec la capacité requise "local/sharedspaceh:accesstospaceh".';
$string['enrolnonrecommendedcapabilities'] = 'La <a href="{$a->link}">méthode d\'inscription de type Temporary Access pour le rôle "{$a->role}"</a> a un réglage de capacité requise non recommandé. Réglage recommandé : "local/sharedspaceh:accesstospaceh".';
$string['enrolnottemporaryaccess'] = 'Au moins une <a href="{$a->link}">méthode d\'inscription</a> trouvée pour le rôle "{$a->role}", mais il est recommandé d\'utiliser une méthode d\'inscription de type Temporary Access.';
$string['enrolok'] = 'La <a href="{$a->link}">méthode d\'inscription de type Temporary Access pour le rôle "{$a->role}"</a> est correctement configurée.';
$string['filtersharedcourses'] = 'Filtrer par cours partagés';

$string['setting:filterlocations'] = 'Afficher un bouton de filtre par cours partagés sur';
$string['setting:filterlocations_desc'] = 'Un bouton permettant aux utilisateurs de filtrer la liste de cours par cours partagés sera ajouté aux emplacements sélectionnés.';
$string['setting:courseindex'] = 'La liste des cours';
$string['setting:courseindexcategory'] = 'La liste des cours par catégorie';
$string['setting:coursesearch'] = 'Le résultat de la recherche de cours';
$string['setting:siteindex'] = 'La liste combinée de cours sur la page d\'accueil';
