<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Checklist for sharing a course to the teaching community.
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../config.php');

global $PAGE, $OUTPUT;

$courseid = required_param('id', PARAM_INT);
$course = get_course($courseid);
$context = context_course::instance($courseid);

require_login();
require_capability('moodle/course:update', $context);

$PAGE->set_course( $course );
$PAGE->set_context( $context );
$PAGE->set_title( format_string($course->fullname) . ' - ' . get_string( 'sharingchecklist', 'local_sharingchecklist' ) );
$PAGE->set_pagelayout( 'incourse' );
$PAGE->set_heading( format_string($course->fullname) );
$PAGE->set_url( '/local/sharingchecklist/index.php', array( 'id' => $courseid ) );

echo $OUTPUT->header();
echo $OUTPUT->heading( get_string( 'sharingchecklist', 'local_sharingchecklist' ) );
echo $OUTPUT->box_start();

(new local_sharingchecklist\checklist($course->id))->print();

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
