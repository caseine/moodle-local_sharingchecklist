<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Callbacks used by Moodle API.
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Add necessary extensions to the course menu.
 * @param navigation_node $navigation Navigation node to add items to.
 * @param object $course Course object.
 * @param context $context Course context.
 */
function local_sharingchecklist_extend_navigation_course($navigation, $course, $context) {
    global $PAGE;

    if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
    }

    if (has_capability('moodle/course:update', $context)) {
        $url = new moodle_url('/local/sharingchecklist/index.php', [ 'id' => $course->id ]);
        $name = get_string('sharingchecklist', 'local_sharingchecklist');
        $navigation->add($name, $url, navigation_node::TYPE_SETTING,
                null, null, new pix_icon('sharing', '', 'local_sharingchecklist'));
    }
}

/**
 * Definition of Fontawesome icons mapping.
 * @return string[] Fontawesome icons mapping.
 */
function local_sharingchecklist_get_fontawesome_icon_map() {
    return [
            'local_sharingchecklist:sharing' => 'fa-share-alt'
    ];
}

function local_sharingchecklist_before_footer() {
    global $PAGE, $DB, $OUTPUT;
    $coursefiltersplugin = core_component::get_plugin_directory('local', 'coursefilters');
    if ($coursefiltersplugin !== null) {
        $locations = explode(',', get_config('local_sharingchecklist', 'filterlocations'));
        if (in_array($PAGE->pagetype, $locations) && has_capability('local/sharedspaceh:accesstospaceh', $PAGE->context)) {
            $shownumber = $PAGE->pagetype != 'course-search';
            $categoryfilter = $PAGE->pagetype == 'course-index-category' ? optional_param('categoryid', null, PARAM_INT) : null;
            $visitablecourses = [];
            $categorieswithcourses = [];
            $sql = "SELECT c.id, cat.path
                      FROM {course} c
                      JOIN {course_categories} cat ON cat.id = c.category
                     WHERE c.visible = 1";
            foreach ($DB->get_records_sql($sql) as $course) {
                if ($categoryfilter !== null && !in_array($categoryfilter, explode('/', $course->path))) {
                    continue;
                }
                if ((new local_sharingchecklist\checklist($course->id))->overall_status() != local_sharingchecklist\checklist_point::FAIL) {
                    $visitablecourses[] = (int) $course->id;
                    foreach (explode('/', $course->path) as $category) {
                        if (!empty($category)) {
                            $categorieswithcourses[(int) $category] = true;
                        }
                    }
                }
            }
            $label = get_string('filtersharedcourses', 'local_sharingchecklist');
            $icon = $OUTPUT->pix_icon('enrolicon', '', 'enrol_temporaryaccess', [ 'class' => 'mx-1 align-self-center' ]);
            $label .= $shownumber ? ' (' . count($visitablecourses) . ')' . $icon : $icon;
            $PAGE->requires->js_call_amd('local_coursefilters/courses_filter', 'init',
                    [ $label, $visitablecourses, array_keys($categorieswithcourses), $shownumber && count($visitablecourses) == 0 ]);
        }
    }
}