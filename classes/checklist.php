<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sharingchecklist;

use moodle_url;

/**
 *
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class checklist {

    protected $points;

    public function __construct($courseid) {
        global $DB;

        $this->points = [];

        $sharingcart = $DB->get_record_sql(
                "SELECT DISTINCT bi.id, COALESCE(bp.visible, 1) as visible
                   FROM {block_instances} bi
              LEFT JOIN {block_positions} bp ON bp.blockinstanceid = bi.id
                   JOIN {context} c ON c.id = bi.parentcontextid
                  WHERE bi.blockname = 'sharing_cart' AND c.contextlevel = :contextlevel AND c.instanceid = :courseid",
                [ 'contextlevel' => CONTEXT_COURSE, 'courseid' => $courseid ],
                IGNORE_MULTIPLE // DB sometimes contains duplicate sharing carts... Ignore these duplicates for this usage.
                );
        $sharingcartexists = ($sharingcart !== false);

        if ($sharingcartexists) {
            $this->points[] = new checklist_point(checklist_point::PASS, 'sharingcartpresent');

            if ($sharingcart->visible == 1) {
                $this->points[] = new checklist_point(checklist_point::PASS, 'sharingcartvisible');
            } else {
                $this->points[] = new checklist_point(checklist_point::FAIL, 'sharingcartnotvisible');
            }

            $instance = block_instance_by_id($sharingcart->id);
            $blockediturl = (new moodle_url('/course/view.php',
                    [ 'id' => $courseid, 'bui_editid' => $sharingcart->id, 'sesskey' => sesskey() ]))->out();
            if (!isset($instance->config->contact) ||
                !isset($instance->config->removeunsharedlinks) ||
                !isset($instance->config->preventunsharedbackup)) {
                $this->points[] = new checklist_point(checklist_point::CHECK, 'sharingcartnotconfigured', [ 'link' => $blockediturl ]);
            } else {
                $this->points[] = new checklist_point(checklist_point::PASS, 'sharingcartconfigured', [ 'link' => $blockediturl ]);
            }
        } else {
            $this->points[] = new checklist_point(checklist_point::FAIL, 'sharingcartnotpresent');
        }

        $nshared = $DB->count_records_sql(
                "SELECT COUNT(*)
                   FROM {course_modules} cm
                   JOIN {local_metadata} m ON m.instanceid = cm.id
                  WHERE cm.course = :courseid AND m.fieldid = :sharedfieldid AND m.data = 1",
                [ 'courseid' => $courseid, 'sharedfieldid' => get_config('block_sharing_cart', 'metadatasharedfield') ]
                );

        $nmodules = $DB->count_records('course_modules', [ 'course' => $courseid ]);

        $percentage = $nmodules == 0 ? 0 : (int)($nshared / $nmodules * 100);

        $this->points[] = new checklist_point(($nshared > 0 ? checklist_point::PASS : checklist_point::CHECK), 'modulesshared', [ 'n' => $nshared, 'percent' => $percentage ]);

        $role = $DB->get_record('role', [ 'id' => get_config('block_sharing_cart', 'visitorrole') ], '*', MUST_EXIST);
        $rolename = role_get_name($role);

        $enrolinstances = $DB->get_records('enrol', [ 'courseid' => $courseid, 'roleid' => $role->id ]);
        $enrolmethodsurl = (new moodle_url('/enrol/instances.php?id=' . $courseid))->out();

        if (count($enrolinstances) > 0) {
            $nactive = 0;
            $nactiveta = 0;
            $nactivetawithcorrectcap = 0;
            $enrolid = null;
            foreach ($enrolinstances as $enrolinstance) {
                if (enrol_is_enabled($enrolinstance->enrol) && $enrolinstance->status == ENROL_INSTANCE_ENABLED) {
                    $nactive ++;
                    if ($enrolinstance->enrol == 'temporaryaccess') {
                        $nactiveta ++;
                        $enrolid = $enrolinstance->id;
                        if ($enrolinstance->customtext1 == 'local/sharedspaceh:accesstospaceh') {
                            $nactivetawithcorrectcap ++;
                        }
                    }
                }
            }
            if ($nactive == 0) {
                $this->points[] = new checklist_point(checklist_point::FAIL, 'enrolinactive',
                        [ 'role' => $rolename, 'n' => count($enrolinstances), 'link' => $enrolmethodsurl ]);
            } else if ($nactive >= 1) {
                if ($nactiveta >= 1) {
                    if ($nactiveta == 1) {
                        $enrolinstanceurl = (new moodle_url('/enrol/editinstance.php',
                                [ 'courseid' => $courseid, 'id' => $enrolid, 'type' => 'temporaryaccess' ]))->out();
                    } else {
                        $enrolinstanceurl = $enrolmethodsurl;
                    }
                    if ($nactivetawithcorrectcap == $nactiveta) {
                        $this->points[] = new checklist_point(checklist_point::PASS, 'enrolok',
                                [ 'role' => $rolename, 'link' => $enrolinstanceurl ]);
                    } else {
                        $this->points[] = new checklist_point(checklist_point::CHECK, 'enrolnonrecommendedcapabilities',
                                [ 'role' => $rolename, 'link' => $enrolinstanceurl ]);
                    }
                } else {
                    $this->points[] = new checklist_point(checklist_point::CHECK, 'enrolnottemporaryaccess',
                            [ 'role' => $rolename , 'link' => $enrolmethodsurl ]);
                }
                if ($nactive > 1) {
                    $this->points[] = new checklist_point(checklist_point::CHECK, 'enrolmultiple',
                            [ 'role' => $rolename, 'link' => $enrolmethodsurl ]);
                }
            }
        } else {
            $this->points[] = new checklist_point(checklist_point::FAIL, 'enrolnone',
                    [ 'role' => $rolename, 'link' => $enrolmethodsurl ]);
        }
    }

    public function print() {
        global $USER;
        if (empty($USER->editing)) {
            echo '<p>' . get_string('turnonediting', 'local_sharingchecklist') . '</p>';
        }
        foreach ($this->points as $point) {
            $point->print();
        }
        echo '<h5 class="mt-2">' . get_string('coursestatus' . $this->overall_status(), 'local_sharingchecklist') . '</h5>';
    }

    public function overall_status() {
        $status = checklist_point::PASS;
        foreach ($this->points as $point) {
            if ($point->get_type() == checklist_point::CHECK) {
                $status = checklist_point::CHECK;
            } else if ($point->get_type() == checklist_point::FAIL) {
                return checklist_point::FAIL;
            }
        }
        return $status;
    }
}
