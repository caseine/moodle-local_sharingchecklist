<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sharingchecklist;

/**
 *
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class checklist_point {
    const PASS = 'pass';
    const CHECK = 'check';
    const FAIL = 'fail';

    protected static $icons = [
            self::PASS => '<i class="text-success fa fa-fw fa-check-circle mr-1"></i>',
            self::CHECK => '<i class="text-warning fa fa-fw fa-warning mr-1"></i>',
            self::FAIL => '<i class="text-danger fa fa-fw fa-times-circle mr-1"></i>',
    ];

    protected $type;
    protected $str;
    protected $info;

    /**
     *
     * @param string $type One of PASS, CHECK or FAIL.
     * @param string $str String identifier.
     * @param string|object|array $info Complementary object for string identifier.
     */
    public function __construct($type, $str, $info = null) {
        $this->type = $type;
        $this->str = $str;
        $this->info = $info;
    }

    /**
     * Print a checklist point with status and a message.
     */
    public function print() {
        echo '<div>' . self::$icons[$this->type] . get_string($this->str, 'local_sharingchecklist', $this->info) . '</div>';
    }

    /**
     *
     */
    public function get_type() {
        return $this->type;
    }
}
