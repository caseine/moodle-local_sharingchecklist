<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration plugin-specific settings definition.
 * @package    local_sharingchecklist
 * @copyright  Astor Bizard, 2022
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $coursefiltersplugin = core_component::get_plugin_directory('local', 'coursefilters');
    if ($coursefiltersplugin !== null) {
        $settings = new admin_settingpage('local_sharingchecklist', get_string('pluginname', 'local_sharingchecklist'));
        $settings->add(new admin_setting_configmultiselect(
                'local_sharingchecklist/filterlocations',
                get_string('setting:filterlocations', 'local_sharingchecklist'),
                get_string('setting:filterlocations_desc', 'local_sharingchecklist'),
                [ 'site-index', 'course-search', 'course-index', 'course-index-category' ],
                [
                        'site-index' => get_string('setting:siteindex', 'local_sharingchecklist'),
                        'course-search' => get_string('setting:coursesearch', 'local_sharingchecklist'),
                        'course-index' => get_string('setting:courseindex', 'local_sharingchecklist'),
                        'course-index-category' => get_string('setting:courseindexcategory', 'local_sharingchecklist'),
                ]
                ));
        $ADMIN->add('localplugins', $settings);
        $settings = null;
    }
}
